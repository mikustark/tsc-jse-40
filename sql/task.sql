--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

-- Started on 2021-06-29 14:32:18

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 203 (class 1259 OID 16427)
-- Name: tm_task; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tm_task (
    id character varying(250) NOT NULL,
    name character varying(250) NOT NULL,
    description character varying(250),
    status character varying(250) NOT NULL,
    project_id character varying(250),
    start_date date,
    finish_date date,
    user_id character varying(250),
    created date
);


ALTER TABLE public.tm_task OWNER TO postgres;

--
-- TOC entry 2994 (class 0 OID 16427)
-- Dependencies: 203
-- Data for Name: tm_task; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tm_task (id, name, description, status, project_id, start_date, finish_date, user_id, created) FROM stdin;
\.


--
-- TOC entry 2861 (class 2606 OID 16434)
-- Name: tm_task task_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tm_task
    ADD CONSTRAINT task_pkey PRIMARY KEY (id);


--
-- TOC entry 2862 (class 2606 OID 16435)
-- Name: tm_task task_project_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tm_task
    ADD CONSTRAINT task_project_fkey FOREIGN KEY (project_id) REFERENCES public.tm_project(id);


--
-- TOC entry 2863 (class 2606 OID 16440)
-- Name: tm_task user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tm_task
    ADD CONSTRAINT "user" FOREIGN KEY (user_id) REFERENCES public.tm_user(id);


-- Completed on 2021-06-29 14:32:18

--
-- PostgreSQL database dump complete
--

