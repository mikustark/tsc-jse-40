package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;

import java.sql.Connection;

public interface IConnectionService {

    @NonNull SqlSession getSqlSession();

}
