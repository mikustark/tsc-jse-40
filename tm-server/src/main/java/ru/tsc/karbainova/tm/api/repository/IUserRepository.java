package ru.tsc.karbainova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import ru.tsc.karbainova.tm.dto.User;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user" +
            "(id, first_name, last_name, middle_name, email, login, role, locked, password_hash)" +
            "VALUES(#{id}, #{first_name}, #{last_name}, #{middle_name}, #{email}, #{login}, #{role}, #{locked}, #{password_hash})")
    void add(
            @Param("id") String id,
            @Param("first_name") String first_name,
            @Param("last_name") String last_name,
            @Param("middle_name") String middle_name,
            @Param("email") String email,
            @Param("login") String login,
            @Param("role") String role,
            @Param("locked") Boolean locked,
            @Param("password_hash") String password_hash
    );

    @Update("UPDATE tm_user" +
            "SET first_name=#{first_name}, last_name=#{last_name}, middle_name=#{middle_name}, " +
            "email=#{email}, login=#{login}, role=#{role}, " +
            "locked=#{locked}, password_hash=#{password_hash} WHERE id = #{id}")
    void update(
            @Param("id") String id,
            @Param("first_name") String first_name,
            @Param("last_name") String last_name,
            @Param("middle_name") String middle_name,
            @Param("email") String email,
            @Param("login") String login,
            @Param("role") String role,
            @Param("locked") Boolean locked,
            @Param("password_hash") String password_hash
    );

    @Delete("SELECT * FROM tm_user WHERE id=#{id} LIMIT 1")
    void removeById(final String id);

    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(column = "email", property = "email"),
            @Result(column = "login", property = "login")
    })
    List<User> findAll();

    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(column = "login", property = "login")
    })
    User findByLogin(final String login);

    @Delete("SELECT * FROM tm_user")
    void clear();

    @Select("SELECT * FROM tm_user WHERE email=#{email} LIMIT 1")
    @Results(value = {
            @Result(column = "email", property = "email"),
            @Result(column = "login", property = "login")
    })
    User findByEmail(final String email);

    @Select("SELECT * FROM tm_user WHERE id=#{id} LIMIT 1")
    @Results(value = {
            @Result(column = "email", property = "email"),
            @Result(column = "login", property = "login")
    })
    User findById(final String id);
}
