package ru.tsc.karbainova.tm.component;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.karbainova.tm.api.repository.ICommandRepository;
import ru.tsc.karbainova.tm.api.service.ICommandService;
import ru.tsc.karbainova.tm.api.service.ILogService;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.endpoint.*;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.system.UnknowCommandException;
import ru.tsc.karbainova.tm.repository.CommandRepository;
import ru.tsc.karbainova.tm.service.CommandService;
import ru.tsc.karbainova.tm.service.LogService;
import ru.tsc.karbainova.tm.service.PropertyService;

import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Bootstrap implements ServiceLocator {

    private final ILogService logService = new LogService();

    private final IPropertyService propertyService = new PropertyService();

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);

    private final TaskEndpointService taskEndpointService = new TaskEndpointService();
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    private final UserEndpointService userEndpointService = new UserEndpointService();
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    private final AuthEndpointService authEndpointService = new AuthEndpointService();
    private final AuthEndpoint authEndpoint = authEndpointService.getAuthEndpointPort();

    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

//    private final CalcEndpointService calcEndpointService = new CalcEndpointService();
//    private final CalcEndpoint calcEndpoint = calcEndpointService.getCalcEndpointPort();

    private static Session session;

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        Bootstrap.session = session;
    }

//    private Scanner scanner = new Scanner(System.in);


    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public @NonNull TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @Override
    public @NonNull ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @Override
    public @NonNull UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @Override
    public @NonNull AdminUserEndpoint getAdminUserEndpoint() {
        return adminUserEndpoint;
    }

    @Override
    public @NonNull AuthEndpoint getAuthEndpoint() {
        return authEndpoint;
    }

    @Override
    public SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    public void start(@NonNull final String... args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        initCommand();
        if (args.length == 0) {
            startInput();
        }
        executeCommandByArg(args[0]);
    }


    public void startInput() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!"exit".equals(command)) {
            try {
                System.out.println("Enter command:");
                command = scanner.nextLine();
                executeCommand(command);
            } catch (Exception e) {
                logService.error(e);
            }

        }
    }

    @SneakyThrows
    public void initCommand() {
        final Reflections reflections = new Reflections("ru.tsc.karbainova.tm.command");
        final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.tsc.karbainova.tm.command.AbstractCommand.class)
                .stream()
//                .sorted(ComparatorCommand.getInstance())
                .collect(Collectors.toList());
        for (@NonNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            registryCommand(clazz.newInstance());
        }
    }

    public void registryCommand(@NonNull final AbstractCommand command) {

        @NonNull final String terminalCommand = command.name();
        @NonNull final String terminalDescription = command.description();
        @Nullable final String terminalArg = command.arg();
        if (terminalCommand.isEmpty()) throw new EmptyNameException();
        if (terminalDescription.isEmpty()) throw new EmptyNameException();
        command.setServiceLocator(this);
        commandService.getCommands().put(terminalCommand, command);
        if (terminalArg == null || terminalArg.isEmpty()) return;
        commandService.getArguments().put(terminalArg, command);

    }

    public void executeCommand(@NonNull final String commandName) {
        if (commandName.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(commandName);
        if (abstractCommand == null) throw new UnknowCommandException(commandName);
        abstractCommand.execute();
    }

    public void executeCommandByArg(@NonNull final String argName) {
        if (argName.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByArg(argName);
        if (abstractCommand == null) throw new UnknowCommandException(argName);
        abstractCommand.execute();
    }

}
